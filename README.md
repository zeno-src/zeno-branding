# Zeno Branding

This repository contains common illustrations used to brand the Zeno code editor.

Each directory inside of this repository is ordered as `vx` where `x` is the current version. Inside of each of these versioned directories should be a metadata-type `README.md`.
